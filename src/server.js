const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 8000;
const nunjucks = require("nunjucks");
const routes = require("./routes/routes");
const session = require('express-session');
const passport = require('passport');

app.use(cors());
app.use(express.static("public"));
app.use(express.json());


nunjucks.configure( 'src/views', {
    autoescape: true,
    cache: false,
    express: app
});



//routes setup



mongoose.connect("mongodb://localhost/kitty", {useNewUrlParser: true})
  .then(() => {

    console.log("Mongoose connected!");
      app.use(passport.initialize());
      app.use(passport.session());

      app.use("/", routes())

      app.listen(PORT, () => {
          console.log("Server started at http://localhost:" + PORT);
      });

  });





