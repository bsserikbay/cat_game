const express = require("express");
const router = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const passport = require('passport');
require("./../../passport/setup");


const createRouter = () => {



    router.get('/', function (req, res) {
        res.render('greeting.njk')
    });

    router.get('/login', function (req, res) {
        res.render('login.njk')
    });

    router.get('/register', function (req, res) {
        res.render('register.njk')
    });

    router.post("/users/register", urlencodedParser, (req, res, next) => {
        passport.authenticate("signup", function(err, user, info) {
            console.log('user', info)
            if (err) {
                return res.status(400).json({ errors: err, info });
            }
            if (!user) {
                return res.status(400).json({ errors: "No user found" });
            }
            req.logIn(user, function(err) {
                if (err) {
                    return res.status(400).json({ errors: err });
                }
                return res.status(200).json({ success: `logged in ${user.id}` });
            });
        })(req, res, next);
    });


    router.post("/users/login", urlencodedParser, (req, res, next) => {

        passport.authenticate('login', async (error, user, info) => {
            try {
                if (error) {
                    return res.status(500).json({
                        message: 'Something is wrong',
                        error: error || 'internal server errror',
                    });
                }
                //req.login is provided by passport to serilize user id
                req.login(user, async (error) => {
                    if (error) {
                        res.status(500).json({
                            message: 'Somthing is wrong',
                            error: error || 'internal server errror',
                        });
                    }

                    return res.send({ user, info });
                });
            } catch (error) {
                return next(error);
            }
        })(req, res, next);


    });

    return router;
};


module.exports = createRouter;
